package com.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Iterator;

import com.interfaces.Category;
import com.interfaces.Part;
import com.interfaces.PartType;

public class PartTypeImpl implements PartType {

	private Collection<Part> parts;
	private Collection<PartType> incompatibilities;
	private Collection<PartType> requirements;
	private String name;
	private String description;
	private Category category;
	private Class<? extends Part> reference;

	public PartTypeImpl(String name, String description, Category category, Class<? extends Part> reference) {
		this.name = name;
		this.description = description;
		this.category = category;
		this.reference  = reference;
	}
	
	public Part newInstance() throws Exception {
		Constructor< ?extends Part>  constructor = this.reference.getConstructor();
		return constructor.newInstance();
	}

	@Override
	public Iterator<Part> getPartsFromPartType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub

	}

}
