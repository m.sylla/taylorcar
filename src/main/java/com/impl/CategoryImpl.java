package com.impl;

import java.util.Collection;
import java.util.Iterator;

import com.interfaces.Category;
import com.interfaces.Part;
import com.interfaces.PartType;

public enum CategoryImpl implements Category {
	
	ENGINE("Engine"),
	TRANSMISSION("Transmission"),
	INTERIOR("Interior"),
	EXTERIOR("Exterior");
	
	private String name;
	private Collection<PartType> partTypes;
	
	private CategoryImpl(String name) {
		this.name  = name;
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<PartType> getPartTypesFromCategory(Category category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<Part> getPartsFromCategory(Category category) {
		// TODO Auto-generated method stub
		return null;
	}

}
