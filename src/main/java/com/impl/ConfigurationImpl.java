package com.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import com.interfaces.Configuration;
import com.interfaces.Observable;
import com.interfaces.Observer;
import com.interfaces.Part;
import com.interfaces.PartType;

public class ConfigurationImpl<T> implements Configuration, Observable<T> {
	
    private List<Observer<T>> registeredObservers;
	private T value;
	private List<Part> parts;

	@Override
	public Iterator<PartType> getIncompatibleParts(PartType partType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterator<PartType> getRequiredParts(PartType partType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isComplete() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isValid() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addPart(Part part) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removePart(Part part) {
		// TODO Auto-generated method stub

	}

	@Override
	public void register(Observer<T> observer) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void unregister(Observer<T> observer) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void setValue(T value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public T getValue() {
		// TODO Auto-generated method stub
		notifyRegisteredObservers();
		return null;
	}

	@Override
	public boolean isRegistered(Observer<T> observer) {
		// TODO Auto-generated method stub
		return false;
	}
	
	private void notifyRegisteredObservers() {
		for (Observer<T> o : registeredObservers) {
			o.update(this);
		}
	}


}
