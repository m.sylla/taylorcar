package com.interfaces;

import java.util.Collection;
import java.util.Iterator;

public interface Configurator {

	public void addPart(Part part);
	public void removePart(Part part);
	public Iterator<Part> getPartsFromCategory(Category category);
	public Part selectPart(Iterator<Part> parts, String name);
	public Configuration getConfiguration();
}
