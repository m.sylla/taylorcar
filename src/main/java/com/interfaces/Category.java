package com.interfaces;

import java.util.Iterator;
import java.util.List;

public interface Category {
	
	public String getName();
	public Iterator<PartType> getPartTypesFromCategory(Category category);
	public Iterator<Part> getPartsFromCategory(Category category);
}
