package com.interfaces;

import java.util.Collection;
import java.util.List;

public interface Observable<T> {
	
	public void register(Observer<T> observer) throws IllegalArgumentException;
	public void unregister(Observer<T> observer) throws IllegalArgumentException;
	public void setValue(T value);
	public T getValue();
	public boolean isRegistered(Observer<T> observer);

}
