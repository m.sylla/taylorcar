package com.interfaces;

import java.util.Iterator;

public interface CompatibilityChecker {
	
	public Iterator<PartType> getIncompatibleParts(PartType partType);
	public Iterator<PartType> getRequiredParts(PartType partType);

}
