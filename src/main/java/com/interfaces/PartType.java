package com.interfaces;

import java.util.Iterator;

public interface PartType {
	
	public Iterator<Part> getPartsFromPartType();
	public String getName();
	public void setName(String name);

}
