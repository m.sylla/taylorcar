package com.interfaces;

public interface Configuration extends CompatibilityChecker{
	
	public boolean isComplete();
	public boolean isValid();
	public void addPart(Part part);
	public void removePart(Part part);

}
