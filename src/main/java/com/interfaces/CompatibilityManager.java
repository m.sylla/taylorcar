package com.interfaces;

import java.util.Iterator;

public interface CompatibilityManager extends CompatibilityChecker{
	
	public void addIncompatibilities(PartType reference, Iterator<PartType> incompatibilities);
	public void addRequirements(PartType reference, Iterator<PartType> requirements);
	public void removeIncompatibilities(PartType reference, Iterator<PartType> incompatibilities);
	public void removeRequirements(PartType reference, Iterator<PartType> requirements);
	

}
