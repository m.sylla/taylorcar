package com.interfaces;

public interface Observer<T> {
	
	public void update(Observable<T> observable);

}
